#!/usr/bin/python3.8

import os, json, base64, subprocess

def exec(cmd, cwd = None, input = None, encoding = None):
    res = subprocess.run(cmd, capture_output = True, cwd = cwd, shell = True, input = input, encoding = encoding)
    result = {
        'result': res.returncode,
        'output': res.stdout,
        'error': res.stderr
    }

    return result

# Temporarily attain submission from a json file.
task = None
with open('task.json', 'r') as f:
    task = json.loads(f.read())

# Save source file into "/judger/submission" directory
for file in task['source']:
    with open('/judger/source/{}'.format(file['filename']), 'w') as f:
        f.write(base64.b64decode(bytes(file['content'], 'utf-8')).decode('utf-8'))

# Compile submission
task['result'] = {'status': 'AC'}
compile = exec("/judger/judger {} {} gcc -c *.c -fmax-errors=5".format(task['compileLimit']['time'], task['compileLimit']['memory']), cwd = "/judger/source")
with open('/judger/source/result.json', 'r') as f:
    result = json.loads(f.read())
    if result['status'] != 'Success':
        task['result']['status'] = 'CE'
        task['result']['error'] = compile['error']

compile = exec("/judger/judger {} {} gcc -o submission /judger/source/*.o -lseccomp -fmax-errors=5".format(task['compileLimit']['time'], task['compileLimit']['memory']), cwd = "/judger/submission")
with open('/judger/submission/result.json', 'r') as f:
    result = json.loads(f.read())
    if result['status'] != 'Success':
        task['result']['status'] = 'CE'
        task['result']['error'] = compile['error']

# Run tasks
if task['result']['status'] != 'CE':
    task['result']['score'] = 0
    task['result']['time'] = 0
    task['result']['memory'] = 0
    for subtask in task['subtasks']:
        result = exec("/judger/judger {} {} ./submission".format(subtask['limit']['time'], subtask['limit']['memory']), input = subtask['input'], cwd = "/judger/submission", encoding = "ascii")

        with open('/judger/submission/result.json') as f:
            resource = json.loads(f.read())

        subtask['result'] = {}
        if resource['status'] != "Success":
            task['result']['status'] = subtask['result']['status'] = resource['status']
            subtask['result']['score'] = 0
        elif result['output'] != subtask['output']:
            task['result']['status'] = subtask['result']['status'] = 'WA'
            subtask['result']['score'] = 0
        else:
            subtask['result']['status'] = 'AC'
            subtask['result']['score'] = subtask['score']

        subtask['result']['time'] = resource['time']
        subtask['result']['memory'] = resource['memory']

        task['result']['score'] += subtask['result']['score']
        task['result']['time'] = max(task['result']['time'], subtask['result']['time'])
        task['result']['memory'] = max(task['result']['memory'], subtask['result']['memory'])

print(json.dumps(task))
