#include <seccomp.h>
#include <linux/seccomp.h>


// install seccomp restriction before executing main()

void __attribute__ ((constructor)) setup_seccomp(){
  scmp_filter_ctx ctx;

  int syscalls_whitelist[] = {
    SCMP_SYS(read), SCMP_SYS(fstat),
    SCMP_SYS(mmap), SCMP_SYS(mprotect),
    SCMP_SYS(munmap), SCMP_SYS(uname),
    SCMP_SYS(arch_prctl), SCMP_SYS(brk),
    SCMP_SYS(access), SCMP_SYS(exit_group),
    SCMP_SYS(close), SCMP_SYS(readlink),
    SCMP_SYS(sysinfo), SCMP_SYS(write),
    SCMP_SYS(writev), SCMP_SYS(lseek),
    SCMP_SYS(clock_gettime), SCMP_SYS(ioctl),
    SCMP_SYS(rt_sigprocmask), SCMP_SYS(fork),
    SCMP_SYS(gettid)
  };

  int syscalls_whitelist_length = sizeof(syscalls_whitelist) / sizeof(int);

  ctx = seccomp_init(SCMP_ACT_KILL);
  for(int i = 0; i < syscalls_whitelist_length; i++)
    seccomp_rule_add(ctx, SCMP_ACT_ALLOW, syscalls_whitelist[i], 0);

  seccomp_load(ctx);
}
