#define _GNU_SOURCE
#include <time.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/resource.h>

static pid_t slave_pid;
static bool TLE, MLE;

static inline unsigned int getTickCount(){
  struct timespec tp;
  assert(clock_gettime(CLOCK_MONOTONIC_RAW, &tp) == 0);

  return tp.tv_sec * 1000u + tp.tv_nsec / 1000000;
}

static void slave(int time_limit, int memory_limit, char *argv[]){
  struct rlimit time_rlim, memory_rlim;
  time_rlim.rlim_cur = time_rlim.rlim_max = time_limit * 1000;
  memory_rlim.rlim_cur = memory_rlim.rlim_max = memory_limit * 1024;

  assert(setrlimit(RLIMIT_RTTIME, &time_rlim) == 0);
  assert(setrlimit(RLIMIT_DATA, &memory_rlim) == 0);

  assert(raise(SIGSTOP) == 0);

  assert(execvp(argv[0], argv) != -1 && 0);
}

static void handler(int sig){
  TLE = true;
  kill(slave_pid, SIGKILL);
}

static void master(int time_limit, int memory_limit){
  int status;
  assert(waitpid(slave_pid, &status, WUNTRACED) == slave_pid);
  assert(WIFSTOPPED(status) && WSTOPSIG(status) == SIGSTOP);

  unsigned int t = getTickCount();
  signal(SIGALRM, handler);

  struct itimerval timer = {};
  timer.it_value.tv_sec = time_limit / 1000;
  timer.it_value.tv_usec = ((long long)time_limit % 1000) * 1000;

  assert(setitimer(ITIMER_REAL, &timer, NULL) == 0);
  assert(kill(slave_pid, SIGCONT) == 0);

  assert(waitpid(slave_pid, &status, 0) == slave_pid);

  struct itimerval cancel = {};
  setitimer(ITIMER_REAL, &cancel, NULL);
  t = getTickCount() - t;

  struct rusage usage;
  assert(getrusage(RUSAGE_CHILDREN, &usage) == 0);
  long int m = usage.ru_maxrss;
  if(m > memory_limit)
    MLE = true;

  FILE *fp = fopen("result.json", "w");
  assert(fp != NULL);

  if(TLE)
    fprintf(fp, "{\"status\": \"TLE\", \"time\": %u, \"memory\": %ld}\n", t, m);
  else if(MLE)
    fprintf(fp, "{\"status\": \"MLE\", \"time\": %u, \"memory\": %ld}\n", t, m);
  else if(WIFSIGNALED(status) && WTERMSIG(status) == SIGSYS)
    fprintf(fp, "{\"status\": \"BSC\", \"time\": %u, \"memory\": %ld}\n", t, m);
  else if(!WIFEXITED(status) || WEXITSTATUS(status) != 0)
    fprintf(fp, "{\"status\": \"RE\", \"time\": %u, \"memory\": %ld}\n", t, m);
  else
    fprintf(fp, "{\"status\": \"Success\", \"time\": %u, \"memory\": %ld}\n", t, m);

  assert(fclose(fp) == 0);
}

int main(int argc, char *argv[]) {
  assert(argc > 3);
  const int time_limit = atoi(argv[1]);     // Time limit in millisecond.
  const int memory_limit = atoi(argv[2]);   // Memory limit in kilobytes.

  assert(time_limit >= 0 && memory_limit >= 0);

  assert(setuid(0) == 0);
  assert(setgid(0) == 0);

  slave_pid = fork();

  if(slave_pid == 0){
    char **args = malloc((argc - 2) * sizeof(char*));
    for(int i = 0; i + 3 < argc; i++)
      args[i] = argv[i + 3];
    args[argc - 3] = NULL;
    slave(time_limit, memory_limit, args);
  }
  assert(slave_pid != -1);
  master(time_limit, memory_limit);

  return 0;
}
