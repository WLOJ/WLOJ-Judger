FROM ubuntu:latest

RUN apt update && apt install -y gcc seccomp libseccomp-dev python3.8
COPY /judger /judger
RUN gcc -o /judger/judger /judger/judger.c && rm -rf /judger/judger.c
WORKDIR /judger
